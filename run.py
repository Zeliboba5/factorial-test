import argparse
import logging

from aiohttp import web

from app.router import setup_routes

LOG_LEVEL_MAP = {
    'CRITICAL': logging.CRITICAL,
    'FATAL': logging.FATAL,
    'ERROR': logging.ERROR,
    'WARN': logging.WARNING,
    'WARNING': logging.WARNING,
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
    'NOTSET': logging.NOTSET,
}


def run(port, logging_level):
    logging.basicConfig(
        format='%(asctime)s %(name)s %(levelname)s %(message)s',
        level=logging_level
    )
    app = web.Application()
    setup_routes(app)
    web.run_app(app, port=port)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', help='Port, where server will be serving')
    parser.add_argument('--loglevel', help='Log level for app')
    args = parser.parse_args()
    server_port = int(args.port) if args.port else 5000
    logging_level_str = args.loglevel
    if logging_level_str and logging_level_str in LOG_LEVEL_MAP:
        logging_level = LOG_LEVEL_MAP[logging_level_str]
    else:
        logging_level = logging.INFO
    run(server_port, logging_level)
