import inspect
import json
import logging
import math
from collections import Mapping

from aiohttp import web, WSMsgType


async def _factorial_handler(request, ws, number: int, *args, **kwargs):
    try:
        result = math.factorial(number)
    except ValueError:
        await ws.send_json({
            'error': f'"number" should be positive integer'
        })
        return
    await ws.send_json({
        'method': 'factorial',
        'payload': {
            'result': result,
        }
    })


WEBSOCKET_METHODS = {
    'factorial': _factorial_handler,
}


async def _handle_websocket_message(message, request, ws):
    method_name = message.get('method')
    if not method_name:
        await ws.send_json({
            'error': 'Request body should define "method" field'
        })
        return
    method = WEBSOCKET_METHODS.get(method_name)
    if not method:
        await ws.send_json({
            'error': f'Method "{method_name}" is unknown'
        })
        return
    payload = message.get('payload')
    if payload is None or not isinstance(payload, Mapping):
        await ws.send_json({
            'error': 'Body should define "payload" field as object'
        })
        return

    try:
        await method(request, ws, **payload)
    except TypeError:
        logging.exception('Error in method call:')
        method_params = inspect.signature(method).parameters.keys()
        required_arguments = (
            set(method_params) - {'request', 'ws', 'args', 'kwargs'}
        )
        await ws.send_json({
            'error': f'Payload should include following fields: {", ".join(required_arguments)}'
        })
    except:
        logging.exception(f'Error in method handling for method {method_name}:')
        method_params = inspect.signature(method).parameters
        method_param_names = method_params.keys()
        required_arguments = (
            set(method_param_names) - {'request', 'ws', 'args', 'kwargs'}
        )
        arguments_with_types = [
            f"{method_params[a].name}: {method_params[a].annotation.__name__}"
            for a in required_arguments
        ]
        await ws.send_json({
            'error': f'Error in method handling, ensure such signature is followed in payload: '
                     f'{", ".join(arguments_with_types)}'
        })


async def websocket_handler(request):
    ws = web.WebSocketResponse()
    await ws.prepare(request)

    async for message in ws:
        if message.type == WSMsgType.TEXT:
            logging.debug(f'Got message from client:\n {message.data}')
            try:
                message_data = json.loads(message.data)
            except json.JSONDecodeError:
                logging.exception(f'Error in decoding user message:\n{message.data}\n')
                await ws.send_json({
                    'error': 'unreadable message'
                })
                continue
            try:
                await _handle_websocket_message(message_data, request, ws)
            except:
                logging.exception('Error in handling user message:')
        elif WSMsgType.ERROR:
            logging.error(ws.exception())

    return ws
