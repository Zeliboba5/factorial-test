import math


async def test_factorial(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'number': 15,
            }
        })
        result = await ws.receive_json()
        assert result['payload']['result'] == math.factorial(15)


async def test_factorial_negative_number(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'number': -1,
            }
        })
        result = await ws.receive_json()
        assert result['error']


async def test_factorial_string(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'number': "test",
            }
        })
        result = await ws.receive_json()
        assert result['error']


async def test_factorial_consecutive(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'number': 15,
            }
        })
        result = await ws.receive_json()
        assert result['payload']['result'] == math.factorial(15)

        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'number': 10,
            }
        })
        result = await ws.receive_json()
        assert result['payload']['result'] == math.factorial(10)


async def test_other_method(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial1',
            'payload': {
                'number': 15,
            }
        })
        result = await ws.receive_json()
        assert result['error']


async def test_no_payload(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
        })
        result = await ws.receive_json()
        assert result['error']


async def test_no_field_in_payload(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {}
        })
        result = await ws.receive_json()
        assert result['error']


async def test_no_field_other_field_in_payload(client):
    async with client.ws_connect('/ws') as ws:
        await ws.send_json({
            'method': 'factorial',
            'payload': {
                'to_calculate': 15
            }
        })
        result = await ws.receive_json()
        assert result['error']