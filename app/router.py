from aiohttp import web

from app.views import websocket_handler


def setup_routes(app):
    app.router.add_routes([web.get('/ws', websocket_handler)])
