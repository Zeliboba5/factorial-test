## Сервис расчёта факториала

### Запуск сервиса

Требуется предварительно сбилдить контейнер

`docker build --tag factorial-test .`

Для запуска сервиса

`docker run -d -p 5000:5000 --name factorial-test factorial-test`

### API

Сервис имеет один эндпоинт:

`/ws`

Данный эндпоинт предоставляет возможность открыть websocket
для дальнейшего общения

#### Формат сообщений

```
{
    "method": <str>,  # Название метода
    "payload": <object>  # Тело сообщения
}
```

Если в ходе обработки сообщения произошла ошибка, то отдаётся сообщение
следующего формата

```
{
    "error": <str>  # Описание ошибки
}
```

#### Доступные методы

`factorial`

Параметры:

`number` - число от котрого требуется посчитать факториал

Пример запроса:
```
{
    "method": "factorial",
    "payload": {
        "number": 5
    }
}
```

Пример ответа:
```
{
    "method": "factorial",
    "payload": {
        "result": 120
    }
}
```